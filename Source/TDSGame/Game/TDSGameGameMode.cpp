// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSGameGameMode.h"
#include "TDSGamePlayerController.h"
#include "../Character/TDSGameCharacter.h"
#include "UObject/ConstructorHelpers.h"

ATDSGameGameMode::ATDSGameGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = ATDSGamePlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/BP_Character"));
	if (PlayerPawnBPClass.Class != nullptr)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	// set default controller to our Blueprinted controller
	static ConstructorHelpers::FClassFinder<APlayerController> PlayerControllerBPClass(TEXT("/Game/Blueprint/Game/BP_PlayerController"));
	if(PlayerControllerBPClass.Class != NULL)
	{
		PlayerControllerClass = PlayerControllerBPClass.Class;
	}
}