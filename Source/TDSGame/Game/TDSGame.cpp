// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TDSGame, "TDSGame" );

DEFINE_LOG_CATEGORY(LogTDSGame)
 