// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSGameCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "Materials/Material.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/EngineTypes.h"
#include "Kismet/KismetMathLibrary.h"

ATDSGameCharacter::ATDSGameCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void ATDSGameCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);
	MovementTick(DeltaSeconds);
}

void ATDSGameCharacter::SetupPlayerInputComponent(UInputComponent* InpComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
	InpComponent->BindAxis(TEXT("MoveForward"), this, &ATDSGameCharacter::InputAxisX);
	InpComponent->BindAxis(TEXT("MoveRight"), this, &ATDSGameCharacter::InputAxisY);

}

void ATDSGameCharacter::InputAxisX(float Value)
{
	AxisX = Value;
}

void ATDSGameCharacter::InputAxisY(float Value)
{
	AxisY = Value;
}

void ATDSGameCharacter::MovementTick(float DeltaTime)
{
	AddMovementInput(FVector(AxisX, AxisY, 0.0f), 1.0f);

	APlayerController* MyController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (MyController)
	{
		FHitResult ResultHit;
		MyController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit);
		SetActorRotation(FQuat(FRotator(0.0f, UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw, 0.0f)));
	}

}

void ATDSGameCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;
	switch (MovementState)
	{
	case EMovementState::Sprint_State:
		ResSpeed = MovementSpeedInfo.SprintSpeed;
		break;
	case EMovementState::Crouch_State:
		ResSpeed = MovementSpeedInfo.CrouchSpeed;
		break;
	case EMovementState::Aim_State:
		ResSpeed = MovementSpeedInfo.AimSpeed;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementSpeedInfo.WalkSpeed;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementSpeedInfo.RunSpeed; 
		break;
	}
	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void ATDSGameCharacter::ChangeMovementState()
{
	if (SprintEnabled)
	{
		MovementState=EMovementState::Sprint_State;
	} else if (CrouchEnabled)
	{
		MovementState = EMovementState::Crouch_State;
	}
	else if(AimEnabled)
	{
		MovementState = EMovementState::Aim_State;
	}
	else if (WalkEnabled)
	{
		MovementState = EMovementState::Walk_State;
	}
	else 
	{
		MovementState = EMovementState::Run_State;
	}
	CharacterUpdate();
}
