// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "MyTypes.generated.h"
/**
 * 
 */

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	Sprint_State UMETA(DisplayName = "Sprint State"),
	Crouch_State UMETA(DisplayName = "Crouch State"),
	Aim_State UMETA(DisplayName="Aim State"),
	Walk_State UMETA(DisplayName = "Walk State"),
	Run_State UMETA(DisplayName = "Run State"),
};

USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float SprintSpeed = 900.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float CrouchSpeed = 200.0f;
	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category= "Movement")
	float AimSpeed = 250.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float WalkSpeed = 300.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float RunSpeed = 600.0f;
};

UCLASS()
class TDSGAME_API UMyTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

};
